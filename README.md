# ME-App

A personal application for practice and showcase Android design/implementation skills.

# Techniques / Utility libraries

## App Architecture

- MVVM
- AndroidViewModel
- LiveData

## UI flow control

- JetPack Navigation (with SafeArgs)
- ConstraintLayout
- CollapsingToolbarLayout

## Remote data storage and analysis tool

- Firebase Storage
- Firebase Crashlytics
- Firebase Analytics

## Restful API：

- Retrofit
- Moshi

## Local Database

- JetPack Room

## Image download/Cache

- Glide

# Design Document

## Mind Map
![mind map](doc/mind_map.jpg)

## UI Mockup

![UI](doc/ui_mockup.png)

## App architecture (MVVM)

![Arch](doc/arch.png)

## DB Schema

![DB Schema](doc/db_schema.png)

# Implementation Screenshot

![implement](doc/app_screen.png)

# Play Store link

https://play.google.com/store/apps/details?id=net.mf99.meapp

