package net.mf99.meapp.entity

data class ArticleData(
    val name: String,
    val link: String
)