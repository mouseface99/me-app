package net.mf99.meapp.entity

data class VersionConfig(
    val parserVersion: Int,
    val dataVersion: Int
)