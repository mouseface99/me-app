package net.mf99.meapp.entity

data class SkillEntry(
    val id: String,
    val name: String,
    val image: String,
    val numOfProjects: Int
)

data class SkillData(
    val id: String,
    val name: String,
    val image: String,
    val description: String,
    val projectList: List<ProjectEntry>
)