package net.mf99.meapp.entity

data class ProjectEntry(
    val id: String,
    val name: String,
    val icon: String,
    val companyName: String
)

data class ProjectData(
    val id: String,
    val name: String,
    val image: String,
    val description: String,
    val responsible: String,
    val skills: List<SkillEntry>
)