package net.mf99.meapp.entity

data class JobEntry(
    val id: String,
    val companyName: String,
    val companyImage: String,
    val title: String,
    val startDate: Long,
    val endDate: Long
)

data class JobData(
    val id: String,
    val companyName: String,
    val companyImage: String,
    val title: String,
    val startDate: Long,
    val endDate: Long,
    val responsible: String,
    val projectList: List<ProjectEntry>
)