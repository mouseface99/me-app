package net.mf99.meapp.repo.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "job_table")
data class JobEntity(
    @PrimaryKey val id: String,
    val title: String,
    @ColumnInfo(name = "company_name") val companyName: String,
    @ColumnInfo(name = "company_icon") val companyIcon: String,
    @ColumnInfo(name = "start_date") val startDate: Long,
    @ColumnInfo(name = "end_date") val endDate: Long,
    val responsible: String
)

@Entity(tableName = "project_table")
data class ProjectEntity(
    @PrimaryKey val id: String,
    val name: String,
    val icon: String,
    val image: String,
    @ColumnInfo(name = "job_id") val jobID: String,
    val description: String,
    val responsible: String
)

@Entity(tableName = "skill_table")
data class SkillEntity(
    @PrimaryKey val id: String,
    val name: String,
    val icon: String,
    val description: String
)

@Entity(tableName = "skill_map_table", primaryKeys = ["project_id", "skill_id"])
data class SkillMapEntity(
    @ColumnInfo(name = "project_id") val projectID: String,
    @ColumnInfo(name = "skill_id") val skillID: String
)

@Entity(tableName = "article_table")
data class ArticleEntity(
    @PrimaryKey val order: Int,
    val name: String,
    val link: String
)