package net.mf99.meapp.repo.remote

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.mf99.meapp.entity.VersionConfig
import net.mf99.meapp.repo.*
import net.mf99.meapp.repo.room.RoomDao

class RemoteRepo(var apiService: APIService, var roomDao: RoomDao) {

    val versionConfig by lazy { MutableLiveData<VersionConfig>() }
    val dataUpdateComplete by lazy { MutableLiveData<Boolean>() }
    private var filename: String? = null

    fun checkVersion(){
        CoroutineScope(Dispatchers.IO).launch {
            val result = apiService.getConfigAsync()
            var config = VersionConfig(0, 0)

            if(result != null){
                config = VersionConfig(
                    result.version.parser,
                    result.version.data
                )
                filename = result.filename
            }

            versionConfig.postValue(config)
        }
    }

    fun downloadNewData(){
        CoroutineScope(Dispatchers.IO).launch {
            var result = false
            if(filename != null){
                val data = apiService.getDataAsync(filename!!)
                result = updateData(data)
            }

            dataUpdateComplete.postValue(result)
        }
    }

    private fun updateData(data: DataModel) : Boolean{
        // Insert data to DB via ROOM
        roomDao.insertJobs(*(data.jobList.map { jobEntityAdapter(it) }.toTypedArray()))
        roomDao.insertProjects(*(data.projectList.map { projectEntityAdapter(it) }.toTypedArray()))
        roomDao.insertSkills(*(data.skillList.map { skillEntityAdapter(it) }.toTypedArray()))
        roomDao.insertSkillMaps(*(data.skillMappingList.map { skillMapEntityAdapter(it) }.toTypedArray()))
        roomDao.insertArticles(*(data.articleList.map { articleEntityAdapter(it) }.toTypedArray()))
        return true
    }
}