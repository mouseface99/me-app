package net.mf99.meapp.repo

import net.mf99.meapp.entity.JobData
import net.mf99.meapp.entity.JobEntry
import net.mf99.meapp.repo.room.RoomDao

class JobRepo(var roomDao: RoomDao) {

    fun getJobList(): List<JobEntry>{
        return roomDao.getJobList().map {
            jobEntryAdapter(it)
        }
    }

    fun getJobDetail(id: String): JobData{
        return jobDataAdapter(
            roomDao.getJobDetail(id),
            roomDao.getProjectListForJob(id)
        )
    }
}