package net.mf99.meapp.repo

import net.mf99.meapp.entity.SkillData
import net.mf99.meapp.entity.SkillEntry
import net.mf99.meapp.repo.room.RoomDao

class SkillRepo(var roomDao: RoomDao) {

    fun getSkillList(): List<SkillEntry>{
        return roomDao.getSkillList().map {
            skillEntryAdapter(it, roomDao.getMapListForSkill(it.id).size)
        }
    }

    fun getSkillDetail(id: String): SkillData{
        return skillDataAdapter(roomDao.getSkillDetail(id),
                roomDao.getProjectListsByIDs(roomDao.getMapListForSkill(id).map { it.projectID })
        )
    }
}