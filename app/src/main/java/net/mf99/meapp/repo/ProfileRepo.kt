package net.mf99.meapp.repo

import net.mf99.meapp.entity.ArticleData
import net.mf99.meapp.repo.room.RoomDao

class ProfileRepo(var roomDao: RoomDao) {

    fun getArticleList(): List<ArticleData>{
        return roomDao.getArticleList().map { articleDataAdapter(it) }
    }
}