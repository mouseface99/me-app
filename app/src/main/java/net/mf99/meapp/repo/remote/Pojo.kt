package net.mf99.meapp.repo.remote

import com.squareup.moshi.JsonClass

// Config

@JsonClass(generateAdapter = true)
data class ConfigModel(
    val version: VersionModel,
    val filename: String,
    val passcode: String
)

@JsonClass(generateAdapter = true)
data class VersionModel(
    val parser: Int,
    val data: Int
)

// Data

@JsonClass(generateAdapter = true)
data class DataModel(
    val jobList: List<JobModel>,
    val projectList: List<ProjectModel>,
    val skillList: List<SkillModel>,
    val skillMappingList: List<MappingModel>,
    val articleList: List<ArticleModel>
)

@JsonClass(generateAdapter = true)
data class JobModel(
    val id: String,
    val title: String,
    val companyName: String,
    val companyIcon: String,
    val startDate: Long,
    val endDate: Long,
    val responsible: String
)

@JsonClass(generateAdapter = true)
data class ProjectModel(
    val id: String,
    val name: String,
    val icon: String,
    val image: String,
    val jobID: String,
    val description: String,
    val responsible: String
)

@JsonClass(generateAdapter = true)
data class SkillModel(
    val id: String,
    val name: String,
    val icon: String,
    val description: String
)

@JsonClass(generateAdapter = true)
data class MappingModel(
    val projectID: String,
    val skillID: String
)

@JsonClass(generateAdapter = true)
data class ArticleModel(
    val order: Int,
    val name: String,
    val link: String
)