package net.mf99.meapp.repo

import net.mf99.meapp.entity.ProjectData
import net.mf99.meapp.entity.ProjectEntry
import net.mf99.meapp.repo.room.RoomDao

class ProjectRepo(var roomDao: RoomDao) {

    fun getProjectList(): List<ProjectEntry>{
        return roomDao.getProjectList().map {
            val job = roomDao.getJobDetail(it.jobID)
            projectEntryAdapter(it, job.companyName)
        }
    }

    fun getProjectDetail(id: String): ProjectData{
        return projectDataAdapter(roomDao.getProjectDetail(id),
            roomDao.getSkillListsByIDs(roomDao.getMapListForProject(id).map { it.skillID })
        )
    }
}