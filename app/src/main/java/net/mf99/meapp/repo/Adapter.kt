package net.mf99.meapp.repo

import net.mf99.meapp.entity.*
import net.mf99.meapp.repo.remote.*
import net.mf99.meapp.repo.room.*

fun jobEntityAdapter(model: JobModel): JobEntity{
    return JobEntity(
        model.id, model.title,
        model.companyName, model.companyIcon,
        model.startDate, model.endDate,
        model.responsible
    )
}

fun jobEntryAdapter(roomEntity: JobEntity): JobEntry {
    return JobEntry(
        roomEntity.id,
        roomEntity.companyName, roomEntity.companyIcon,
        roomEntity.title,
        roomEntity.startDate, roomEntity.endDate
    )
}

fun jobDataAdapter(roomEntity: JobEntity, projectList: List<ProjectEntity>): JobData {
    return JobData(
        roomEntity.id,
        roomEntity.companyName, roomEntity.companyIcon,
        roomEntity.title,
        roomEntity.startDate, roomEntity.endDate,
        roomEntity.responsible,
        projectList.map { projectEntryAdapter(it, roomEntity.companyName) }
    )
}

fun projectEntityAdapter(model: ProjectModel): ProjectEntity{
    return ProjectEntity(
        model.id,
        model.name, model.icon, model.image,
        model.jobID,
        model.description, model.responsible
    )
}

fun projectEntryAdapter(roomEntity: ProjectEntity, jobName: String): ProjectEntry {
    return ProjectEntry(
        roomEntity.id,
        roomEntity.name, roomEntity.icon,
        jobName
    )
}

fun projectDataAdapter(roomEntity: ProjectEntity, skillList: List<SkillEntity>): ProjectData{
    return ProjectData(
        roomEntity.id, roomEntity.name, roomEntity.image,
        roomEntity.description, roomEntity.responsible,
        skillList.map { skillEntryAdapter(it, 0) }
    )
}

fun skillEntityAdapter(model: SkillModel): SkillEntity{
    return SkillEntity(
        model.id,
        model.name, model.icon,
        model.description
    )
}

fun skillMapEntityAdapter(model: MappingModel): SkillMapEntity{
    return SkillMapEntity(model.projectID, model.skillID)
}

fun skillEntryAdapter(roomEntity: SkillEntity, projectCount: Int): SkillEntry {
    return SkillEntry(
        roomEntity.id,
        roomEntity.name, roomEntity.icon,
        projectCount
    )
}

fun skillDataAdapter(roomEntity: SkillEntity, projectList: List<ProjectEntity>): SkillData {
    return SkillData(
        roomEntity.id, roomEntity.name, roomEntity.icon,
        roomEntity.description,
        projectList.map { projectEntryAdapter(it, "FIX ME !?") }
    )
}

fun articleEntityAdapter(model: ArticleModel): ArticleEntity{
    return ArticleEntity(
        model.order,
        model.name,
        model.link
    )
}

fun articleDataAdapter(roomEntity: ArticleEntity): ArticleData {
    return ArticleData(
        roomEntity.name,
        roomEntity.link
    )
}