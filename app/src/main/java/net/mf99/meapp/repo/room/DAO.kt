package net.mf99.meapp.repo.room

import android.content.Context
import androidx.room.*

@Database(entities = [JobEntity::class, ProjectEntity::class, SkillEntity::class, SkillMapEntity::class, ArticleEntity::class],
    version = 1, exportSchema = false)
abstract class MyDatabase: RoomDatabase(){
    abstract fun getRoomDao(): RoomDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: MyDatabase? = null

        fun getDatabase(context: Context): MyDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MyDatabase::class.java,
                    "me-app"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}

@Dao
interface RoomDao {
    // Job
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertJobs(vararg jobs: JobEntity)

    @Query("SELECT * from job_table ORDER BY end_date DESC")
    fun getJobList(): List<JobEntity>

    @Query("SELECT * from job_table WHERE id=:jobID")
    fun getJobDetail(jobID: String): JobEntity

    // Project
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProjects(vararg projects: ProjectEntity)

    @Query("SELECT * from project_table")
    fun getProjectList(): List<ProjectEntity>

    @Query("SELECT * from project_table WHERE job_id=:jobID")
    fun getProjectListForJob(jobID: String): List<ProjectEntity>

    @Query("SELECT * from project_table WHERE id=:projectID")
    fun getProjectDetail(projectID: String): ProjectEntity

    @Query("SELECT * from project_table WHERE id in (:idList)")
    fun getProjectListsByIDs(idList: List<String>): List<ProjectEntity>

    // Skill
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSkills(vararg skills: SkillEntity)

    @Query("SELECT * from skill_table")
    fun getSkillList(): List<SkillEntity>

    @Query("SELECT * from skill_table where id=:skillID")
    fun getSkillDetail(skillID: String): SkillEntity

    @Query("SELECT * from skill_table WHERE id in (:idList)")
    fun getSkillListsByIDs(idList: List<String>): List<SkillEntity>

    // Mapping
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSkillMaps(vararg mappingList : SkillMapEntity)

    @Query("SELECT * FROM skill_map_table WHERE skill_id=:skillID")
    fun getMapListForSkill(skillID: String): List<SkillMapEntity>

    @Query("SELECT * FROM skill_map_table WHERE project_id=:projectID")
    fun getMapListForProject(projectID: String): List<SkillMapEntity>

    // Article
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertArticles(vararg article : ArticleEntity)

    @Query("SELECT * FROM article_table ORDER by `order`")
    fun getArticleList(): List<ArticleEntity>
}