package net.mf99.meapp.repo.remote

import retrofit2.http.GET
import retrofit2.http.Path

interface APIService {
    @GET("/v0/b/meapp-mf99.appspot.com/o/me_config.json?alt=media")
    suspend fun getConfigAsync(): ConfigModel

    @GET("/v0/b/meapp-mf99.appspot.com/o/data%2F{filename}?alt=media")
    suspend fun getDataAsync(
        @Path("filename") filename: String
    ) : DataModel
}