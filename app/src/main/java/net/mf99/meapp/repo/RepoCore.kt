package net.mf99.meapp.repo

import android.content.Context
import net.mf99.meapp.repo.remote.APIService
import net.mf99.meapp.repo.remote.RemoteRepo
import net.mf99.meapp.repo.room.MyDatabase
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RepoCore {

    private val retrofit: Retrofit by lazy { Retrofit.Builder()
        .baseUrl("https://firebasestorage.googleapis.com")
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
    }

    private var jobRepo: JobRepo? = null
    private var projectRepo: ProjectRepo? = null
    private var skillRepo: SkillRepo? = null
    private var profileRepo: ProfileRepo? = null
    private var remoteRepo: RemoteRepo? = null

    fun getProjectRepo(ctx: Context): ProjectRepo{
        if (projectRepo == null)
            projectRepo = ProjectRepo(MyDatabase.getDatabase(ctx).getRoomDao())

        return projectRepo!!
    }

    fun getJobRepo(ctx: Context): JobRepo{
        if (jobRepo == null)
            jobRepo = JobRepo(MyDatabase.getDatabase(ctx).getRoomDao())

        return jobRepo!!
    }

    fun getSkillRepo(ctx: Context): SkillRepo{
        if (skillRepo == null)
            skillRepo = SkillRepo(MyDatabase.getDatabase(ctx).getRoomDao())

        return skillRepo!!
    }

    fun getProfileRepo(ctx: Context): ProfileRepo{
        if (profileRepo == null)
            profileRepo = ProfileRepo(MyDatabase.getDatabase(ctx).getRoomDao())

        return profileRepo!!
    }

    fun getRemoteRepo(ctx: Context): RemoteRepo{
        if (remoteRepo == null)
            remoteRepo = RemoteRepo(
                retrofit.create(APIService::class.java),
                MyDatabase.getDatabase(ctx).getRoomDao()
            )

        return remoteRepo!!
    }
}