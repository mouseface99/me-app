package net.mf99.meapp.view.career

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_job_item.view.*
import net.mf99.meapp.JobDiffCallback
import net.mf99.meapp.R
import net.mf99.meapp.dateFormat
import net.mf99.meapp.entity.JobEntry

class JobListAdapter(
    private var inflater: LayoutInflater,
    private var container: ViewGroup?,
    private var listener: (JobEntry) -> Unit?
) : ListAdapter<JobEntry, JobViewHolder>(JobDiffCallback){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobViewHolder {
        var view = inflater.inflate(R.layout.view_job_item, container, false)
        return JobViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: JobViewHolder, position: Int) {
        holder.setData(getItem(position))
    }
}


class JobViewHolder(var view: View, var listener: (JobEntry) -> Unit?) : RecyclerView.ViewHolder(view){
    fun setData(data: JobEntry){
        view.tv_company_name.text = data.companyName
        view.tv_title_name.text = data.title
        view.tv_duration.text = "${dateFormat(data.startDate)} ~ ${dateFormat(data.endDate)}"
        Glide.with(view).load(data.companyImage).into(view.iv_company_icon)
        view.setOnClickListener{
            listener.invoke(data)
        }
    }
}