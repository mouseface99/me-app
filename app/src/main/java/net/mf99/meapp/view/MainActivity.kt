package net.mf99.meapp.view

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*
import net.mf99.meapp.R

class MainActivity : AppCompatActivity() {

    private var menuBar: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupNav()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        when (item.itemId) {
            android.R.id.home -> navController.navigateUp()
            R.id.go_home -> navController.navigate(R.id.action_to_home)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.action_bar_menu, menu)
        menuBar = menu
        return true
    }

    private fun setupNav() {
        val navController = findNavController(R.id.nav_host_fragment)

        // Passing each menu ID as a set of Ids because each menu should be considered as top level destinations.
        val topDestSet = setOf(
            R.id.navigation_career,
            R.id.navigation_skills,
            R.id.navigation_projects,
            R.id.navigation_profile
        )

        val appBarConfiguration = AppBarConfiguration(topDestSet)
        setupActionBarWithNavController(navController, appBarConfiguration)
        nav_view.setupWithNavController(navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                in(topDestSet) -> {
                    nav_view.visibility = View.VISIBLE
                    menuBar?.findItem(R.id.go_home)?.isVisible = false
                }
                else -> {
                    nav_view.visibility = View.GONE
                    menuBar?.findItem(R.id.go_home)?.isVisible = true
                }
            }
        }
    }
}
