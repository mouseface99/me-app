package net.mf99.meapp.view.project

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_project_skill_item.view.*
import net.mf99.meapp.ProjectDiffCallback
import net.mf99.meapp.R
import net.mf99.meapp.entity.ProjectEntry

class ProjectListAdapter(
    private var inflater: LayoutInflater,
    private var container: ViewGroup?,
    private var listener: (ProjectEntry) -> Unit
): ListAdapter<ProjectEntry, ProjectViewHolder>(ProjectDiffCallback){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProjectViewHolder {
        var view = inflater.inflate(R.layout.view_project_skill_item, container, false)
        return ProjectViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: ProjectViewHolder, position: Int) {
        holder.setData(getItem(position))
    }
}

class ProjectViewHolder(var view: View, var listener: (ProjectEntry) -> Unit) : RecyclerView.ViewHolder(view){
    fun setData(data: ProjectEntry){
        view.tv_title.text = data.name
        view.tv_subtitle.text = view.context.getString(R.string.company_name, data.companyName)
        Glide.with(view).load(data.icon).into(view.iv_icon)
        view.setOnClickListener{
            listener.invoke(data)
        }
    }
}