package net.mf99.meapp.view.career

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.mf99.meapp.entity.JobData
import net.mf99.meapp.entity.JobEntry
import net.mf99.meapp.repo.RepoCore
import net.mf99.meapp.view.BaseViewModel

class CareerViewModel(application: Application) : BaseViewModel(application) {

    val jobList by lazy { MutableLiveData<List<JobEntry>>() }
    val jobData by lazy { MutableLiveData<JobData>() }

    fun fetchJobList() {
        viewModelScope.launch(Dispatchers.IO) {
            val list = RepoCore.getJobRepo(getApplication()).getJobList()
            jobList.postValue(list)
        }
    }

    fun fetchJobData(id: String){
        viewModelScope.launch(Dispatchers.IO) {
            var data = RepoCore.getJobRepo(getApplication()).getJobDetail(id)
            jobData.postValue(data)
        }
    }
}