package net.mf99.meapp.view.project

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.mf99.meapp.entity.ProjectData
import net.mf99.meapp.entity.ProjectEntry
import net.mf99.meapp.repo.RepoCore
import net.mf99.meapp.view.BaseViewModel

class ProjectsViewModel(application: Application) : BaseViewModel(application){

    val projectList by lazy { MutableLiveData<List<ProjectEntry>>() }
    val projectData by lazy { MutableLiveData<ProjectData>() }

    fun fetchProjectList() {
        viewModelScope.launch(Dispatchers.IO) {
            val list = RepoCore.getProjectRepo(getApplication()).getProjectList()
            projectList.postValue(list)
        }
    }

    fun fetchProjectData(id: String){
        viewModelScope.launch(Dispatchers.IO) {
            var data = RepoCore.getProjectRepo(getApplication()).getProjectDetail(id)
            projectData.postValue(data)
        }
    }
}