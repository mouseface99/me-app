package net.mf99.meapp.view.profile

import android.graphics.Color
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_portfolio_item.view.*
import net.mf99.meapp.R
import net.mf99.meapp.entity.ArticleData


class PortfolioAdapter(
    private var inflater: LayoutInflater,
    private var container: ViewGroup?,
    private var listener: (ArticleData) -> Unit
): ListAdapter<ArticleData, PortfolioViewHolder>(PortfolioDiffCallback){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PortfolioViewHolder {
        var view = inflater.inflate(R.layout.view_portfolio_item, container, false)
        return PortfolioViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: PortfolioViewHolder, position: Int) {
        holder.setData(getItem(position))
    }
}

class PortfolioViewHolder(var view: View, var listener: (ArticleData) -> Unit) : RecyclerView.ViewHolder(view){
    fun setData(data: ArticleData){
        if(data.link.isBlank()) {
            view.tv_name.text = data.name
            view.setBackgroundColor(Color.LTGRAY)
        }
        else {
            view.tv_name.text = "・${data.name}"
            view.setBackgroundColor(Color.WHITE)
        }

        view.setOnClickListener{
            listener.invoke(data)
        }
    }
}

object PortfolioDiffCallback : DiffUtil.ItemCallback<ArticleData>() {
    override fun areItemsTheSame(oldItem: ArticleData, newItem: ArticleData): Boolean {
        return oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: ArticleData, newItem: ArticleData): Boolean {
        return oldItem.name == newItem.name
    }
}