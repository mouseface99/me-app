package net.mf99.meapp.view.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_profile.view.*
import net.mf99.meapp.R

const val LINK_LINKED_IN = "https://linkedin.com/in/mike-wang-48774720/"
const val LINK_GITLAB = "https://gitlab.com/mouseface99/"
const val LINK_BLOG = "https://mouseface99.hatenablog.com/"
const val LINK_RESUME = "https://www.cakeresume.com/wen-kai-wang"

class ProfileFragment : Fragment() {
    private val viewModel: ProfileViewModel by viewModels()
    private lateinit var adapter: PortfolioAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view =  inflater.inflate(R.layout.fragment_profile, container, false)
        adapter = PortfolioAdapter(inflater, container) {
            if(it.link.isNotBlank()){
                launchURL(it.link)
            }
        }
        view.rv_portfolio_list.adapter = adapter
        view.iv_linkedin.setOnClickListener {launchURL(LINK_LINKED_IN)}
        view.iv_gitlab.setOnClickListener {launchURL(LINK_GITLAB)}
        view.iv_blog.setOnClickListener {launchURL(LINK_BLOG)}
        view.iv_resume.setOnClickListener {launchURL(LINK_RESUME)}

        viewModel.articleList.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.fetchProfileData()
    }

    private fun launchURL(url: String){
        val action = ProfileFragmentDirections.actionNavigationProfileToWebViewFragment(url)
        findNavController().navigate(action)
    }
}
