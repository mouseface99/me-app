package net.mf99.meapp.view.skill

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_list_page.view.*
import net.mf99.meapp.R

class SkillsFragment : Fragment() {

    private val viewModel: SkillsViewModel by viewModels()

    private lateinit var adapter: SkillListAdapter

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list_page, container, false)
        adapter = SkillListAdapter(inflater, container) {
            val action = SkillsFragmentDirections.actionNavigationToSkillDetailFragment(it.id, it.name)
            findNavController().navigate(action)
        }
        view.rv_list.adapter = adapter

        viewModel.skillList.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.fetchSkillList()
    }
}
