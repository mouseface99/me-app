package net.mf99.meapp.view.skill

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_skill_detail.*
import kotlinx.android.synthetic.main.fragment_skill_detail.view.*
import net.mf99.meapp.R
import net.mf99.meapp.SimpleProjectAdapter
import net.mf99.meapp.entity.SkillData

/**
 * An example full-screen fragment that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class SkillDetailFragment : Fragment() {
    private val args: SkillDetailFragmentArgs by navArgs()
    private val viewModel: SkillsViewModel by viewModels()
    private lateinit var adapter: SimpleProjectAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view =  inflater.inflate(R.layout.fragment_skill_detail, container, false)
        adapter = SimpleProjectAdapter(inflater, container) {
            var action = SkillDetailFragmentDirections.actionSkillDetailFragmentToProjectDetailFragment(it.id, it.name)
            findNavController().navigate(action)
        }
        view.rv_project_list.adapter = adapter

        viewModel.skillData.observe(viewLifecycleOwner, Observer {
            updateData(it)
        })

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.fetchSkillData(args.skillId)
    }

    private fun updateData(data: SkillData){
        tv_name.text = data.name
        tv_description.text = data.description
        Glide.with(this).load(data.image).into(iv_skill_icon)
        adapter.submitList(data.projectList)
    }
}