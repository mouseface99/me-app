package net.mf99.meapp.view.skill

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.mf99.meapp.entity.SkillData
import net.mf99.meapp.entity.SkillEntry
import net.mf99.meapp.repo.RepoCore
import net.mf99.meapp.view.BaseViewModel

class SkillsViewModel(application: Application) : BaseViewModel(application){

    val skillList by lazy { MutableLiveData<List<SkillEntry>>() }
    val skillData by lazy { MutableLiveData<SkillData>() }

    fun fetchSkillList() {
        viewModelScope.launch(Dispatchers.IO) {
            val list = RepoCore.getSkillRepo(getApplication()).getSkillList()
            skillList.postValue(list)
        }
    }

    fun fetchSkillData(id: String){
        viewModelScope.launch(Dispatchers.IO) {
            var data = RepoCore.getSkillRepo(getApplication()).getSkillDetail(id)
            skillData.postValue(data)
        }
    }
}