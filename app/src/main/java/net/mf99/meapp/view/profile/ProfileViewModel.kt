package net.mf99.meapp.view.profile

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.mf99.meapp.entity.ArticleData
import net.mf99.meapp.repo.RepoCore
import net.mf99.meapp.view.BaseViewModel

class ProfileViewModel(application: Application) : BaseViewModel(application){

    val articleList by lazy { MutableLiveData<List<ArticleData>>() }

    fun fetchProfileData(){
        viewModelScope.launch(Dispatchers.IO) {
            var data = RepoCore.getProfileRepo(getApplication()).getArticleList()
            articleList.postValue(data)
        }
    }
}