package net.mf99.meapp.view.career

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_job_detail.*
import kotlinx.android.synthetic.main.fragment_job_detail.view.*
import net.mf99.meapp.R
import net.mf99.meapp.SimpleProjectAdapter
import net.mf99.meapp.dateFormat
import net.mf99.meapp.entity.JobData

/**
 * An example full-screen fragment that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class JobDetailFragment : Fragment() {
    private val args: JobDetailFragmentArgs by navArgs()
    private val viewModel: CareerViewModel by viewModels()
    private lateinit var adapter: SimpleProjectAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view =  inflater.inflate(R.layout.fragment_job_detail, container, false)
        adapter = SimpleProjectAdapter(inflater, container) {
            var action = JobDetailFragmentDirections.actionJobDetailFragmentToProjectDetailFragment(it.id, it.name)
            findNavController().navigate(action)
        }
        view.rv_project_list.adapter = adapter

        viewModel.jobData.observe(viewLifecycleOwner, Observer {
            updateData(it)
        })

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.fetchJobData(args.jobId)
    }

    private fun updateData(data: JobData){
        val ctx = requireContext()
        tv_title.text = data.title
        tv_company.text = ctx.getString(R.string.company_name, data.companyName)
        Glide.with(this).load(data.companyImage).into(iv_company_icon)
        tv_duration.text = ctx.getString(R.string.duration, dateFormat(data.startDate), dateFormat(data.endDate))
        tv_responsibility.text = data.responsible
        adapter.submitList(data.projectList)
    }
}