package net.mf99.meapp.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_launcher.*
import net.mf99.meapp.PARSER_VERSION
import net.mf99.meapp.R
import net.mf99.meapp.repo.RepoCore


class LauncherActivity: AppCompatActivity() {

    private val preference by lazy { getSharedPreferences("version", Context.MODE_PRIVATE) }
    private var dataComplete = false
    private var animationComplete = false
    private var stateChecker = MutableLiveData<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)

        RepoCore.getRemoteRepo(this).versionConfig.observe(this, Observer {
            if(isSameParserVersion(it.parserVersion)){
                if(isNewerDataVersion(it.dataVersion)){// There's new Data, update DB first
                    RepoCore.getRemoteRepo(this).dataUpdateComplete.observe(this, Observer {
                        dataComplete = true
                        stateChecker.postValue(1)
                    })
                    RepoCore.getRemoteRepo(this).downloadNewData()
                }
                else {// there's no new data, keep proceed
                    dataComplete = true
                    stateChecker.postValue(2)
                }
            }
            else{// Parser version not match, need to update App first
                tv_warning.visibility = View.VISIBLE
                tv_warning.animate().apply {
                    alpha(1F)
                    start()
                }
            }
        })

        stateChecker.observe(this, Observer {
            if(dataComplete && animationComplete) {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        })
    }

    override fun onResume() {
        super.onResume()

        iv_icon.animate().apply {
            startDelay = 300
            duration = 1500
            alpha(1.0F)
            withEndAction {
                animationComplete = true
                stateChecker.postValue(3)
            }
            start()
        }

        RepoCore.getRemoteRepo(this).checkVersion()
    }

    private fun isSameParserVersion(newVersion: Int) : Boolean{
        return newVersion == PARSER_VERSION
    }

    private fun isNewerDataVersion(newVersion: Int): Boolean{
        if(newVersion > 0){
            val currentVersion = preference.getInt("DATA_VERSION", -1)
            if(currentVersion != newVersion){
                preference.edit {
                    putInt("DATA_VERSION", newVersion)
                    apply()
                }
                return true
            }
        }
        return false
    }

}