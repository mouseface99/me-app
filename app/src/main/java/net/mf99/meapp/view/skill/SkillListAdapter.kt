package net.mf99.meapp.view.skill

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_project_skill_item.view.*
import net.mf99.meapp.R
import net.mf99.meapp.SkillDiffCallback
import net.mf99.meapp.entity.SkillEntry

class SkillListAdapter(
    private var inflater: LayoutInflater,
    private var container: ViewGroup?,
    private var listener: (SkillEntry) -> Unit
): ListAdapter<SkillEntry, SkillViewHolder>(SkillDiffCallback){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SkillViewHolder {
        var view = inflater.inflate(R.layout.view_project_skill_item, container, false)
        return SkillViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: SkillViewHolder, position: Int) {
        holder.setData(getItem(position))
    }
}

class SkillViewHolder(var view: View, var listener: (SkillEntry) -> Unit) : RecyclerView.ViewHolder(view){
    fun setData(data: SkillEntry){
        view.tv_title.text = data.name
        view.tv_subtitle.text = view.context.resources.getQuantityString(R.plurals.skill_in_projects, data.numOfProjects, data.numOfProjects)
        Glide.with(view).load(data.image).into(view.iv_icon)
        view.setOnClickListener{
            listener.invoke(data)
        }
    }
}