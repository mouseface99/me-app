package net.mf99.meapp.view.project

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_project_detail.*
import kotlinx.android.synthetic.main.fragment_project_detail.view.*
import net.mf99.meapp.R
import net.mf99.meapp.SimpleSkillAdapter
import net.mf99.meapp.entity.ProjectData

/**
 * An example full-screen fragment that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class ProjectDetailFragment : Fragment() {
    private val args: ProjectDetailFragmentArgs by navArgs()
    private val viewModel: ProjectsViewModel by viewModels()
    private lateinit var adapter: SimpleSkillAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view =  inflater.inflate(R.layout.fragment_project_detail, container, false)
        view.collapsingToolbar.title = args.projectName

        adapter = SimpleSkillAdapter(inflater, container) {
            var action = ProjectDetailFragmentDirections.actionProjectDetailFragmentToSkillDetailFragment(it.id, it.name)
            findNavController().navigate(action)
        }
        view.rv_skill_list.adapter = adapter

        viewModel.projectData.observe(viewLifecycleOwner, Observer {
            updateData(it)
        })

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.fetchProjectData(args.projectId)
    }

    private fun updateData(data: ProjectData){
        tv_features.text = data.description
        tv_responsibility.text = data.responsible
        Glide.with(this).load(data.image).into(iv_product_image)
        adapter.submitList(data.skills)
    }
}