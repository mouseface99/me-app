package net.mf99.meapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_simple_entity_item.view.*
import net.mf99.meapp.entity.JobEntry
import net.mf99.meapp.entity.ProjectEntry
import net.mf99.meapp.entity.SkillEntry
import java.text.SimpleDateFormat
import java.util.*

const val PARSER_VERSION = 1

fun dateFormat(time: Long): String{
    return DateFormat.format(Date(time))
}

private val DateFormat: SimpleDateFormat by lazy {
    SimpleDateFormat("yyyy/MM")
}

class SimpleProjectAdapter(
    private var inflater: LayoutInflater,
    private var container: ViewGroup?,
    private var listener: (ProjectEntry) -> Unit
): ListAdapter<ProjectEntry, SimpleProjectViewHolder>(ProjectDiffCallback){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleProjectViewHolder {
        var view = inflater.inflate(R.layout.view_simple_entity_item, container, false)
        return SimpleProjectViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: SimpleProjectViewHolder, position: Int) {
        holder.setData(getItem(position))
    }
}

class SimpleProjectViewHolder(var view: View, var listener: (ProjectEntry) -> Unit) : RecyclerView.ViewHolder(view){
    fun setData(data: ProjectEntry){
        view.tv_title.text = "・${data.name}"
        Glide.with(view).load(data.icon).into(view.iv_icon)
        view.setOnClickListener{
            listener.invoke(data)
        }
    }
}

class SimpleSkillAdapter(
    private var inflater: LayoutInflater,
    private var container: ViewGroup?,
    private var listener: (SkillEntry) -> Unit
): ListAdapter<SkillEntry, SimpleSkillViewHolder>(SkillDiffCallback){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleSkillViewHolder {
        var view = inflater.inflate(R.layout.view_simple_entity_item, container, false)
        return SimpleSkillViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: SimpleSkillViewHolder, position: Int) {
        holder.setData(getItem(position))
    }
}

class SimpleSkillViewHolder(var view: View, var listener: (SkillEntry) -> Unit) : RecyclerView.ViewHolder(view){
    fun setData(data: SkillEntry){
        view.tv_title.text = "・${data.name}"
        Glide.with(view).load(data.image).into(view.iv_icon)
        view.setOnClickListener{
            listener.invoke(data)
        }
    }
}

object ProjectDiffCallback : DiffUtil.ItemCallback<ProjectEntry>() {
    override fun areItemsTheSame(oldItem: ProjectEntry, newItem: ProjectEntry): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: ProjectEntry, newItem: ProjectEntry): Boolean {
        return oldItem.id == newItem.id
    }
}

object JobDiffCallback : DiffUtil.ItemCallback<JobEntry>() {
    override fun areItemsTheSame(oldItem: JobEntry, newItem: JobEntry): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: JobEntry, newItem: JobEntry): Boolean {
        return oldItem.id == newItem.id
    }
}

object SkillDiffCallback : DiffUtil.ItemCallback<SkillEntry>() {
    override fun areItemsTheSame(oldItem: SkillEntry, newItem: SkillEntry): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: SkillEntry, newItem: SkillEntry): Boolean {
        return oldItem.id == newItem.id
    }
}